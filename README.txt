1. Add a series of images to /testImages folder, labeled in order
ex. 01.<filetype>, 02.<filetype>, ..., 10.<filetype> 

2. Run master_skel.m

Remark: The script will output a 3D point cloud of the reconstructed scene. 
Works best with 3 to 4 images as bundle adjustment is not fully
functional and leads to accumlated error over several frames. 

See www.thekevinchoi.com for more information on the Structure from Motion code. 